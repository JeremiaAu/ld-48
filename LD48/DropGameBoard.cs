﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD48
{
    public class DropGameBoard
    {
        public GamePiece this[Vector vector]
        {
            get => board[vector.X, vector.Y];
            set
            {
                board[vector.X, vector.Y] = value;

                //redraw
                value.Draw(vector, outside, topMargin);
            }
        }
        private int outside;
        private int roundsTillDrop;
        private int currentRound = 0;
        private int topMargin = 1;

        private StringBuilder stringBuilder = new StringBuilder();
        private Random random = new Random();

        public GamePiece[,] board = new GamePiece[30, 30];
        /* x,y ➡
         * ⬇
         */


        private Vector playerPosition;


        private GamePiece player = new GamePiece('@', ConsoleColor.Black, ConsoleColor.Blue);
        private GamePiece rock = new GamePiece('#', ConsoleColor.DarkGray, ConsoleColor.Gray);
        private GamePiece air = new GamePiece(' ', ConsoleColor.Black, ConsoleColor.Black);

        private GamePiece[] allUniqueGamePieces;
        private GamePiece[] allEnvironmentPieces;

        private bool successfullyMovedBeforeUpdate = false;
        public bool GameOver = false;
        public bool previousLineEmpty = false;

        public DropGameBoard()
        {
            outside = board.GetLength(0);
            allUniqueGamePieces = new GamePiece[] { player, rock, air };
            allEnvironmentPieces = new GamePiece[] { rock, air };

            //fill with air
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    board[x, y] = air;
                }
            }

            //fill randomly
            GamePiece[] pieces;
            for (int x = 4; x < board.GetLength(0); x++)
            {
                pieces = generateNewLine();
                insertLine(pieces, x);
            }

            playerPosition = new Vector(3, board.GetLength(1) / 2);
            this[playerPosition] = player;


            //draw outline

            Console.CursorTop = 0;
            Console.CursorLeft = 0;
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(new string('#', board.GetLength(1) + 2));

            Console.BackgroundColor = rock.BackgroundColor;
            Console.ForegroundColor = rock.ForegroundColor;

            for (int x = 0; x < board.GetLength(0); x++)
            {
                Console.CursorTop = x + 1;

                Console.CursorLeft = 0;
                Console.Write(rock.Character);

                Console.CursorLeft = board.GetLength(1) + 1;
                Console.Write(rock.Character);
            }
            ReDrawUpdate();
        }

        private GamePiece[] generateNewLine()
        {
            GamePiece[] newGamePieces = new GamePiece[board.GetLength(1)];

            for (int y = 0; y < board.GetLength(1); y++)
            {
                switch (random.Next(3))
                {
                    case int n when (n <= 1):
                        newGamePieces[y] = air;
                        break;
                    case 2:
                        newGamePieces[y] = rock;
                        break;
                }
            }
            return newGamePieces;
        }

        private GamePiece[] generateNewLineNEW()
        {
            GamePiece[] newGamePieces = new GamePiece[board.GetLength(1)];


            if (previousLineEmpty)
            {
                List<int> exits = new List<int>();
                if (random.Next(0, 2) == 1)
                {
                    exits.Add(random.Next(0, board.GetLength(1)));
                    exits.Add(random.Next(0, board.GetLength(1)));
                }
                else
                {
                    exits.Add(random.Next(0, board.GetLength(1)));
                }
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    if (exits.Contains(y))
                    {
                        newGamePieces[y] = air;
                    }
                    else
                    {
                        newGamePieces[y] = rock;
                    }
                }
                previousLineEmpty = false;
            }
            else
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    newGamePieces[y] = air;
                }
                previousLineEmpty = true;
            }



            return newGamePieces;
        }

        private void insertLine(GamePiece[] line, int to)
        {
            for (int y = 0; y < board.GetLength(1); y++)
            {
                board[to, y] = line[y];
            }
        }

        private void copyLine(int from, int to)
        {
            for (int y = 0; y < board.GetLength(1); y++)
            {
                board[to, y] = board[from, y];
                //board[to, y].Draw(to, y, outside);
            }
        }

        public void Update()
        {
            if (!successfullyMovedBeforeUpdate)
            {
                //fall down
                Vector newPosition = new Vector(playerPosition.X + 1, playerPosition.Y);
                if (playerPosition.X != board.GetLength(0) - 1 && this[newPosition] == air)
                {
                    this[playerPosition] = air;
                    this[newPosition] = player;
                    playerPosition = newPosition;
                }
            }

            roundsTillDrop = (board.GetLength(0) - playerPosition.X - 10) / 3;

            if (currentRound >= roundsTillDrop)
            {
                for (int x = 1; x < board.GetLength(0); x++)
                {
                    copyLine(x, x - 1);
                }
                GamePiece[] newLine = generateNewLine();
                insertLine(newLine, board.GetLength(0) - 1);
                playerPosition.X -= 1;
                currentRound = 0;


                Console.CursorTop = topMargin;
                Console.CursorLeft = 0;
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(new string('#', board.GetLength(1) + 2));

                DrawNewLine(newLine);

                topMargin++;

                //ReDrawUpdate();
                //int i = 1;
                //foreach (GamePiece currentPiece in newLine)
                //{
                //    currentPiece.Draw(board.GetLength(1) - 1, i, outside, topMargin);
                //    i++;
                //}
            }
            else
            {
                currentRound++;
            }

            //game ending criteria

            if (playerPosition.X < 0 || playerPosition.X >= board.GetLength(0) - 1)
            {
                GameOver = true;
                int cursorYStartPosition = (board.GetLength(1) / 2) - (" GAME OVER ".Length / 2);
                int cursorXStartPosition = playerPosition.X + topMargin;

                Console.CursorTop = cursorXStartPosition;
                Console.CursorLeft = cursorYStartPosition;
                Console.Write(" GAME OVER ");

                Console.CursorTop = cursorXStartPosition + 1;
                Console.CursorLeft = cursorYStartPosition;
                Console.Write("           ");

                Console.CursorTop = cursorXStartPosition + 2;
                Console.CursorLeft = cursorYStartPosition;
                Console.Write(" RESTART? ");

                Console.CursorTop = cursorXStartPosition + 3;
                Console.CursorLeft = cursorYStartPosition;
                Console.Write(" PRESS [R] ");


                Console.CursorTop = outside + topMargin;
            }

            successfullyMovedBeforeUpdate = false;
        }

        public void ReDrawUpdate()
        {
            int yBeginning = 0;
            int streak = 0;

            foreach (GamePiece currentPiece in allUniqueGamePieces)
            {
                Console.BackgroundColor = currentPiece.BackgroundColor;
                Console.ForegroundColor = currentPiece.ForegroundColor;

                for (int x = 0; x < board.GetLength(0); x++)
                {
                    Console.CursorTop = x + topMargin;

                    for (int y = 0; y < board.GetLength(1); y++)
                    {
                        if (board[x, y] == currentPiece)
                        {
                            if (streak == 0)
                            {
                                yBeginning = y;
                            }
                            streak++;
                        }
                        else if (streak != 0)
                        {
                            Console.CursorLeft = yBeginning + 1;
                            Console.Write(new string(currentPiece.Character, streak));
                            streak = 0;
                        }
                    }

                    Console.CursorLeft = yBeginning + 1;
                    Console.Write(new string(currentPiece.Character, streak));
                    streak = 0;
                }
            }
            Console.CursorTop = outside + topMargin;
            Console.ResetColor();
        }

        private void DrawNewLine(GamePiece[] newline)
        {
            int yBeginning = 0;
            int streak = 0;

            foreach (GamePiece currentPiece in allUniqueGamePieces)
            {
                Console.BackgroundColor = currentPiece.BackgroundColor;
                Console.ForegroundColor = currentPiece.ForegroundColor;

                Console.CursorTop = board.GetLength(0) + topMargin;

                for (int y = 0; y < board.GetLength(1); y++)
                {
                    if (newline[y] == currentPiece)
                    {
                        if (streak == 0)
                        {
                            yBeginning = y;
                        }
                        streak++;
                    }
                    else if (streak != 0)
                    {
                        Console.CursorLeft = yBeginning + 1;
                        Console.Write(new string(currentPiece.Character, streak));
                        streak = 0;
                    }
                }

                Console.CursorLeft = yBeginning + 1;
                Console.Write(new string(currentPiece.Character, streak));
                streak = 0;
            }

            Console.BackgroundColor = rock.BackgroundColor;
            Console.ForegroundColor = rock.ForegroundColor;

            Console.CursorLeft = 0;
            Console.Write(rock.Character);

            Console.CursorLeft = board.GetLength(1) + 1;
            Console.Write(rock.Character);

            Console.CursorTop = outside + topMargin;
            Console.ResetColor();
        }

        public string Draw()
        {
            stringBuilder.Clear();

            stringBuilder.AppendLine(new String('#', board.GetLength(1) + 2));
            for (int x = 0; x < board.GetLength(0); x++)
            {
                stringBuilder.Append("#");
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    stringBuilder.Append(board[x, y]);
                }
                stringBuilder.AppendLine("#");
            }

            return stringBuilder.ToString();
        }

        public void MoveLeft()
        {
            Vector newPosition = new Vector(playerPosition.X, playerPosition.Y - 1);
            if (playerPosition.Y != 0 && this[newPosition] == air)
            {
                this[playerPosition] = air;
                this[newPosition] = player;
                playerPosition = newPosition;
                successfullyMovedBeforeUpdate = true;
            }

            Console.CursorTop = outside + topMargin;
            Console.CursorLeft = 5;
        }

        public void MoveRight()
        {
            Vector newPosition = new Vector(playerPosition.X, playerPosition.Y + 1);
            if (playerPosition.Y != board.GetLength(1) - 1 && this[newPosition] == air)
            {
                this[playerPosition] = air;
                this[newPosition] = player;
                playerPosition = newPosition;
                successfullyMovedBeforeUpdate = true;
            }
            Console.CursorTop = outside + topMargin;
            Console.CursorLeft = 5;
        }

        public void Jump()
        {
            Vector newPosition = new Vector(playerPosition.X - 1, playerPosition.Y);
            if (playerPosition.X != 0 && this[newPosition] == air)
            {
                //jump one high
                this[playerPosition] = air;
                this[newPosition] = player;
                playerPosition = newPosition;
                successfullyMovedBeforeUpdate = true;
            }
            else
            {
                //can't jump
            }

            Console.CursorTop = outside + topMargin;
            Console.CursorLeft = 5;
        }

        public void Bomb()
        {
            /* radius:
             * 
             *  #
             * # #
             *  #
             */

            Vector newAir1 = new Vector(playerPosition.X - 1, playerPosition.Y);
            Vector newAir2 = new Vector(playerPosition.X + 1, playerPosition.Y);
            Vector newAir3 = new Vector(playerPosition.X, playerPosition.Y - 1);
            Vector newAir4 = new Vector(playerPosition.X, playerPosition.Y + 1);

            this[newAir1] = air;
            this[newAir2] = air;
            this[newAir3] = air;
            this[newAir4] = air;
        }
    }
}