﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace LD48
{
    class Program
    {
        public static DropGameBoard gameBoard;
        public static Timer updateTimer;
        public static long timePerFrame = 200; //milliseconds
        static void Main(string[] args)
        {
            Console.CursorVisible = false;


        start:
            Console.Clear();
            gameBoard = new DropGameBoard();

            Console.SetWindowSize(gameBoard.board.GetLength(1) + 3, gameBoard.board.GetLength(0) + 5);
            Console.SetBufferSize(gameBoard.board.GetLength(1) + 3, gameBoard.board.GetLength(0) * 1000 + 5);

            SetUpdateTimer();

            while (!gameBoard.GameOver)
            {
                if (!GameInput())
                    goto start;
            }

            updateTimer.Dispose();
            updateTimer = null;

            //wait for player to press R
            while (Console.ReadKey().Key != ConsoleKey.R) { }
            goto start;
        }

        private static void SetUpdateTimer()
        {
            if (updateTimer != null)
            {
                updateTimer.Dispose();
            }
            // Create a timer with a two second interval.
            updateTimer = new Timer(timePerFrame);
            // Hook up the Elapsed event for the timer. 
            updateTimer.Elapsed += OnTimedEvent;
            updateTimer.AutoReset = true;
            updateTimer.Enabled = true;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            gameBoard.Update();
        }

        public static bool GameInput()
        {
            if (Console.KeyAvailable)
            {
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.LeftArrow:
                        gameBoard.MoveLeft();
                        break;
                    case ConsoleKey.RightArrow:
                        gameBoard.MoveRight();
                        break;
                    case ConsoleKey.UpArrow:
                        gameBoard.Jump();
                        break;
                    case ConsoleKey.DownArrow:
                        gameBoard.Bomb();
                        break;
                    case ConsoleKey.R:
                        return false;
                }

                //make sure buffer is empty
                while (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.DownArrow)
                {
                    //Console.ReadKey();
                }
            }
            return true;
        }
    }
}
