﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD48
{
    public class GamePiece
    {
        public GamePiece(char character, ConsoleColor backgroundColor, ConsoleColor foregroundColor)
        {
            Character = character;
            BackgroundColor = backgroundColor;
            ForegroundColor = foregroundColor;
        }

        public char Character { get; set; }
        public ConsoleColor BackgroundColor { get; set; }
        public ConsoleColor ForegroundColor { get; set; }

        public override string ToString()
        {
            return Character.ToString();
        }

        public void Draw(int x, int y, int outside, int topMargin)
        {
            Console.CursorTop = x + topMargin;
            Console.CursorLeft = y + 1;

            Console.BackgroundColor = BackgroundColor;
            Console.ForegroundColor = ForegroundColor;
            Console.Write(Character.ToString());

            Console.CursorTop = outside + topMargin;
            Console.ResetColor();
        }

        public void Draw(Vector vector, int outside, int topMargin)
        {
            Draw(vector.X, vector.Y, outside, topMargin);
        }
    }
}
